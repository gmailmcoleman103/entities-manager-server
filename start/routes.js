'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.on('/').render('welcome')
Route.group(() => {
    Route.get('customers', 'CustomerController.index')
    Route.post('customer', 'CustomerController.store')
    Route.get('customer', 'CustomerController.index')
    Route.post('customer/:customer_id', 'CustomerController.show')
    Route.put('customer/:customer_id', 'CustomerController.update')
    Route.delete('customer/:customer_id', 'CustomerController.delete')

    // Route.get('customer/:customer_id/sites', 'SiteController.index')
}).prefix('api/v1')

Route.group(() => {
    Route.get('sites', 'SiteController.index')
    Route.post('site', 'SiteController.store')
    Route.get('site', 'SiteController.index')
    Route.post('site/:site_id', 'SiteController.show')
    Route.put('site/:site_id', 'SiteController.update')
    Route.delete('site/:site_id', 'SiteController.delete')
}).prefix('api/v1')


Route.group(() => {
    Route.get('meters', 'SiteController.index')
    Route.post('meter', 'SiteController.store')
    Route.get('meter', 'SiteController.index')
    Route.post('meter/:meter_id', 'SiteController.show')
    Route.put('meter/:meter_id', 'SiteController.update')
    Route.delete('meter/:meter_id', 'SiteController.delete')
}).prefix('api/v1')
