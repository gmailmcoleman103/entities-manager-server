'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SitesSchema extends Schema {
  up () {
    this.create('sites', (table) => {
      table.increments()
      table.string('name').unique()
      table.float('longitude')
      table.float('latitude')
      table.string('address')
      table.string('postcode')
      table.string('customer')
      table.timestamps()
    })
  }

  down () {
    this.drop('sites')
  }
}

module.exports = SitesSchema
