'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CircuitsSchema extends Schema {
  up () {
    this.create('circuits', (table) => {
      table.increments()
      table.string('name').unique()
      table.date('installationDate')
      table.boolean('isMain')
      table.string('meter')
      table.string('circuitParent')
      table.timestamps()
    })
  }

  down () {
    this.drop('circuits')
  }
}

module.exports = CircuitsSchema
