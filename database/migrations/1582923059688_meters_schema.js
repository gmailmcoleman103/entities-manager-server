'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MetersSchema extends Schema {
  up () {
    this.create('meters', (table) => {
      table.increments()
      table.string('name').unique()
      table.string('serialNumber').unique()
      table.date('installationDate')
      table.string('site')
      table.timestamps()
    })
  }

  down () {
    this.drop('meters')
  }
}

module.exports = MetersSchema
