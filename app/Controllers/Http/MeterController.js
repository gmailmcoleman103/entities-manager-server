'use strict'

const Meter = use('App/Models/Meter')

class MeterController {
    async index({response}){
        
        try{
            const query = request.get()
            let meters = await Meter
            .query()
            .where('site', '=', query.site_id)
            .fetch()
            
            return response.json(meters)
        }catch(ex){
            return response.status(400).json(ex.message)
        }
    }

    async store({request, response}){
        const meterInfo = request.only(["name","serialNumber", "installationDate", "site"])

        const meter = new Meter()
        meter.name = meterInfo.name
        meter.serialNumber = meterInfo.serialNumber
        meter.installationDate = meterInfo.installationDate
        meter.site = meterInfo.site

        await meter.save()

        return response.status(201).json(meter)
    }

    async find({params, response}){
        const meter = await Meter.find(params.meter_id)
        return response.json(meter)
    }

    async update({params, request, response}){

        const meterInfo = request.only(["name","serialNumber", "installationDate", "site"])

        const meter = await Meter.find(params.meter_id)

        if(!meter){
            return response.status(404).json({data: 'Meter not found'})
        }

        meter.name = meterInfo.name
        meter.serialNumber = meterInfo.serialNumber
        meter.installationDate = meterInfo.installationDate
        meter.site = meterInfo.site

        await meter.save()

        return response.status(200).json(meter)
    }

    async delete({params, response}){
        const meter = await Meter.find(params.meter_id)
        if(!meter){
            return response.status(404).json({data: 'Meter not found'})
        }

        await meter.delete()

        return response.status(200).json(null)
    }
}

module.exports = MeterController
