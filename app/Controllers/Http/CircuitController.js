'use strict'

class CircuitController {
    async index({response}){
        
        try{
            const query = request.get()
            let circuits = await Circuit
            .query()
            .where('meter', '=', query.meter_id)
            .fetch()
            
            return response.json(circuits)
        }catch(ex){
            return response.status(400).json(ex.message)
        }
    }

    async store({request, response}){
        const circuitInfo = request.only(["name","installationDate", "isMain", "meter","circuitParent"])

        const circuit = new Circuit()
        circuit.name = circuitInfo.name
        circuit.installationDate = circuitInfo.installationDate
        circuit.meter = circuitInfo.meter
        circuit.isMain = circuitInfo.isMain
        circuit.circuitParent = circuitInfo.circuitParent

        await circuit.save()

        return response.status(201).json(circuit)
    }

    async find({params, response}){
        const circuit = await Circuit.find(params.circuit_id)
        return response.json(circuit)
    }

    async update({params, request, response}){

        const circuitInfo = request.only(["name","installationDate", "isMain", "meter","circuitParent"])
        const circuit = await Circuit.find(params.circuit_id)

        if(!circuit){
            return response.status(404).json({data: 'Circuit not found'})
        }

        circuit.name = circuitInfo.name
        circuit.installationDate = circuitInfo.installationDate
        circuit.meter = circuitInfo.meter
        circuit.isMain = circuitInfo.isMain
        circuit.circuitParent = circuitInfo.circuitParent

        await circuit.save()

        return response.status(200).json(circuit)
    }

    async delete({params, response}){
        const circuit = await Meter.find(params.circuit_id)
        if(!circuit){
            return response.status(404).json({data: 'Circuit not found'})
        }

        await circuit.delete()

        return response.status(200).json(null)
    }
}

module.exports = CircuitController
