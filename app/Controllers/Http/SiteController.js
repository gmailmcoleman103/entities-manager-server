'use strict'

const Site = use('App/Models/Site')

class SiteController {
    async index({ request, response}){
    
        try{
            const query = request.get()
            let sites = await Site
            .query()
            .where('customer', '=', query.customer_id)
            .fetch()
            
            return response.json(sites)
        }catch(ex){
            return response.status(400).json(ex.message)
        }
    }

    async store({request, response}){
        
        try{
            const siteInfo = request.only(["name","address", "postcode", "latitude", "longitude","customer"])

            const site = new Site()
            site.name = siteInfo.name
            site.address = siteInfo.address
            site.postcode = siteInfo.postcode
            site.latitude = siteInfo.latitude
            site.longitude = siteInfo.longitude
            site.customer = siteInfo.customer

            await site.save()

            return response.status(201).json(site)
        }catch(ex){
            return response.status(400).json(ex.message)
        }
    }

    async find({params, response}){
        const site = await Site.find(params.site_id)
        return response.json(site)
    }

    async update({params, request, response}){

        const siteInfo = request.only(["name","address", "postcode", "latitude", "longitude","customer"])

        const site = await Site.find(params.site_id)

        if(!site){
            return response.status(404).json({data: 'Site not found'})
        }

        site.name = siteInfo.name
        site.address = siteInfo.address
        site.postcode = siteInfo.postcode
        site.latitude = siteInfo.latitude
        site.longitude = siteInfo.longitude
        site.customer = siteInfo.customer

        await site.save()

        return response.status(200).json(site)
    }

    async delete({params, response}){
        const site = await Site.find(params.site_id)
        if(!site){
            return response.status(404).json({data: 'Site not found'})
        }

        await site.delete()

        return response.status(200).json(null)
    }
}

module.exports = SiteController
