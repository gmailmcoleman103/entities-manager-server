'use strict'

const Customer = use('App/Models/Customer')

class CustomerController {
    async index({response}){
        let customers = await Customer.all()
        
        return response.json(customers)
    }

    async store({request, response}){
        const customerInfo = request.only(["name","email", "vatNumber"])

        const customer = new Customer()
        customer.name = customerInfo.name
        customer.email = customerInfo.email
        customer.vatNumber = customerInfo.vatNumber

        await customer.save()

        return response.status(201).json(customer)
    }

    async find({params, response}){
        const customer = await Customer.find(params.customer_id)
        return response.json(customer)
    }

    async update({params, request, response}){

        const customerInfo = request.only(["name","email", "vatNumber"])

        const customer = await Customer.find(params.customer_id)

        if(!customer){
            return response.status(404).json({data: 'Customer not found'})
        }

        customer.name = customerInfo.name
        customer.email = customerInfo.email
        customer.vatNumber = customerInfo.vatNumber

        await customer.save()

        return response.status(200).json(customer)
    }

    async delete({params, response}){
        const customer = await Customer.find(params.customer_id)
        if(!customer){
            return response.status(404).json({data: 'Customer not found'})
        }

        await customer.delete()

        return response.status(200).json(null)
    }
}

module.exports = CustomerController
