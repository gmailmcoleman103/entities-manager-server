'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Circuit extends Model {
    static get table () {
        return 'circuits'
    }

    static get primaryKey () {
        return 'id'
    }
}

module.exports = Circuit
