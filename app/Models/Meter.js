'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Meter extends Model {
    static get table () {
        return 'meters'
    }

    static get primaryKey () {
        return 'id'
    }
}

module.exports = Meter
